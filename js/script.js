/**
 * @file A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function($, Drupal, window, document, undefined) {

	// Place your code here.

})(jQuery, Drupal, this, this.document);


jQuery(document).ready(function() {
	jQuery('.jq-etape').each(function(i) {
			jQuery('#jq-etape-'+i+' h2').click(function() {
				if (jQuery('#jq-etape-'+i+' .field').css('display') == 'none') {
					jQuery('#jq-etape-'+i+' .field').show('medium');
					jQuery('#jq-etape-'+i+' article').css('padding-bottom','1em');
					jQuery('#jq-etape-'+i+' span').css('padding-bottom','0.5em');
					jQuery('#jq-etape-'+i+' h2').css("background", "url('/sites/all/themes/robinDebrouille/img/bg-h4-etape-ouverte.png') no-repeat");
					jQuery('.jq-etape').each(function(j) {
						if (i!=j){
							jQuery('#jq-etape-'+j+' .field').hide('medium');
							jQuery('#jq-etape-'+j+' article').css('padding-bottom','0em');
							jQuery('#jq-etape-'+j+' span').css('padding-bottom','0em');
							jQuery('#jq-etape-'+j+' h2').css("background", "url('/sites/all/themes/robinDebrouille/img/bg-h4-etape-fermee.png') no-repeat");
						}
					});
				} else {
					jQuery('#jq-etape-'+i+' .field').hide('medium');
					jQuery('#jq-etape-'+i+' article').css('padding-bottom','0em');
					jQuery('#jq-etape-'+i+' span').css('padding-bottom','0em');
					jQuery('#jq-etape-'+i+' h2').css("background", "url('/sites/all/themes/robinDebrouille/img/bg-h4-etape-fermee.png') no-repeat");
				}
				return false;
			});
	});
});
//block contact
jQuery(document).ready(function() {
	jQuery('#icone_contact').click(function() {
		if (jQuery('#block-block-3').css('display') == 'none') {
			jQuery('#block-block-3').show('medium');
		}
	});
	jQuery('#block-block-3 h2').click(function() {
		jQuery('#block-block-3').hide('medium');
	});
});
//block search
jQuery(document).ready(function() {
	jQuery('#icone_search').click(function() {
		if (jQuery('#block-search-form').css('display') == 'none') {
			jQuery('#block-search-form').show('medium');
		}
	});
	jQuery('#block-search-form h2').click(function() {
		jQuery('#block-search-form').hide('medium');
	});
});
//block user
jQuery(document).ready(function() {
	jQuery('#icone_user').click(function() {
		if (jQuery('#block-user-login').css('display') == 'none') {
			jQuery('#block-user-login').show('medium');
		}
	});
	jQuery('#block-user-login h2').click(function() {
		jQuery('#block-user-login').hide('medium');
	});
});

jQuery(document).ready(function() {
	jQuery('#icone_user_moderateur').click(function() {
		if (jQuery('#block-menu-menu-moderateur').css('display') == 'none') {
			jQuery('#block-menu-menu-moderateur').show('medium');
		}
	});
	jQuery('#block-menu-menu-moderateur h2').click(function() {
		jQuery('#block-menu-menu-moderateur').hide('medium');
	});
});

jQuery(document).ready(function() {
	jQuery('#icone_user_connect').click(function() {
		if (jQuery('#block-system-user-menu').css('display') == 'none') {
			jQuery('#block-system-user-menu').show('medium');
		}
	});
	jQuery('#block-system-user-menu h2').click(function() {
		jQuery('#block-system-user-menu').hide('medium');
	});
});
/* desactivée les lien
jQuery(document).ready(function() {
	jQuery('.group-informations-principales a').click(function() {return false;});
});
jQuery(document).ready(function() {
	jQuery('.group-informations-comps a').click(function() {return false;});
});

function transfer_auto(){
    jQuery('.image-widget-data input.form-submit').css("visibility", "hidden");
    jQuery('.image-widget-data input.form-file').change(function() {
        jQuery(this).parent().find('input.form-submit').mousedown();
    });
}
jQuery(document).ready(function() {
	transfer_auto();
});*/
